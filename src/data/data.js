import logo from './../assets/images/Jb.svg';
import fl from  './../assets/images/flecha.svg';
import me from './../assets/images/mephoto.png';
import one from './../assets/images/One.svg';
import Two from './../assets/images/Two.svg';
import Three from './../assets/images/Three.svg';
import Four from './../assets/images/Four.svg';
import facebook from './../assets/images/facebook.svg';
import Linkedin from './../assets/images/linkedin.svg';
import Correo from './../assets/images/correo.svg';
import Temporal from './../assets/images/Temporal.png';
import gitlab  from './../assets/images/gitlab.png'; 
import python  from './../assets/images/piton.png';
import java  from './../assets/images/java.png';
import c  from './../assets/images/csharp.png';
import react  from './../assets/images/react.svg';
import django  from './../assets/images/django.svg';
import springboot  from './../assets/images/springboot.svg';
import tailwind from './../assets/images/tailwind..svg';
import aprendizaje from './../assets/images/Aprendisaje.png';
import arrow from './../assets/images/arrow.svg';
import boy from './../assets/images/chico.png';
import conta from './../assets/images/contactos.png';
import lap from './../assets/images/lapiz.png';
import camino from './../assets/images/camino.png';
import portafolio from './../assets/images/portafolio.png';

export const HeaderData = {
  logo: {
    title: 'UCC',
    link: 'index.html',
    icon: logo,
    arrow:arrow,
    linkHero:'#hero',
  },

}
 
export const HeroData = {
    title: 'Hola Soy Esteban',
    subTitle:'Software Developer',
    linkMe : '#me',
    linkBriefcase : '#briefcase',
    linkContact : '#contact',
    linkExperience : '#experience',
    linkSkills : '#skills',
    Image:boy,
    imagetwo:conta,
    lapiz:lap,
    camio:camino,
    portfolio:portafolio

  };


  export const MeData = {
    me: {
      title: 'foto presonal',
      link: 'index.html',
      icon: me,
    },

    fl: {
      title: 'Flecha Boton',
      link: 'index.html',
      icon: fl,
    },

    title: 'Sobre Mi',
    text:'Soy Esteban Getial Ingeniero De Software Y Amante De La Programacion. Apasionado del Futbol junto con las peliculas de todo tipo, me encanta viajar a lado de las personas que mas quiero. ademas de que me gustan mucho superar los retos    ',
    
    note:'Aprendo Con Talento Y Compromiso'
   
  };

  export const ExperienceData = {
    header: {
      title: 'Camino De Aprendizaje',
      subTitle:
        'Lo que he aprendido para estar en tu equipo',
        icon: aprendizaje,
    },
    items: [
      {
        id: 1,
        icon: one,
        title: 'Ingeniero de Software',
        subTitle:
          'Estudiante de Ingeneria de Software en Quinto Semestre con experiencia en desarrollo web.',
        
      },
      {
        id: 2,
        icon: Two,
        title: 'Java Curso Interface',
        subTitle:
          'Curso practico de 6 meses realizado por la academy SofwareJunior  ',
       
      },
      {
        id: 3,
        icon: Three,
        title: 'Curso Python 3.0 FABE',
        subTitle:
          'Curso intencivo con FABE en conjunto SENA de 2 meses .',
     
      },
      {
        id: 4,
        icon: Four,
        title: 'Tecnico Electronico Sena',
        subTitle:
          'mantenimiento de equipos electronicos con 1 año de experiencia.',
     
      },
    ],
  };
  
  export const ContactData = {
    header: {
      title: 'Contacto',
      subTitle:'@Jeyban37',
      note:'Me puedes encontrar aqui'
    },
    items: [
      {
        id: 1,
        icon: facebook,
        title: 'Facebook',
        subTitle:
          'Encontraras informacion personal',
        link : 'https://www.facebook.com/jeisson.getial.04/'
        
      },
      {
        id: 2,
        icon: Linkedin,
        title: 'Linkedin',
        subTitle:
          'Aqui mi perfil laboral ',
          link : 'https://www.linkedin.com/in/jeysson-esteban-getial-gualguan-292519219/'
       
      },
      {
        id: 3,
        icon: Correo,
        title: 'Correo',
        subTitle:
          'Enviame un correo siempre estoy atento a mensajes por este medio',
          link : '#hero'
     
      },
    ],
  };


  export const skillsData = {
    header: {
      title: 'Habilidades',
      subTitle:'Aqui encontraras mis habilidades como developer',
      
    },
  
    items: [
      {
        id: 1,
        icon: python,
        title: 'Python',
        subTitle:
          'Llevo 2 años de experiencia trabajando con python un framework que me ha brindado mucho aprendisaje en la Programacion orientada a objetos',
       
        
      },
      {
        id: 2,
        icon:java,
        title: 'Java',
        subTitle:
          'Conoci Java por los videojuegos y me auyudo mucho a comprender la logica de los algoritmos ',
         
      },
      {
        id: 3,
        icon: react,
        title: 'React jsx',
        subTitle:
          'Manejo y comprendo su sintaxis de manera eficaz',
        
     
      },

      {
        id: 4,
        icon: tailwind,
        title: 'Tailwind CSS',
        subTitle:
          'He realizado maquetacion y diseño recientes aplicandolas a mis proyectos REST personales ',
        
      },
      {
        id: 5,
        icon: c,
        title: 'C#',
        subTitle:
          'Desarrollo de aplicacion locales con logica compleja',
        
     
      },
      {
        id: 6,
        icon: django,
        title: 'Django',
        subTitle:
          'He creado Backends funcionales de manera rapida para proyectos personales ',
        
     
      },
      {
        id: 7,
        icon: springboot,
        title: 'SpringBoot',
        subTitle:
          'Enviame un correo siempre estoy atento a mensajes por este medio',
        
     
      },
    ],}





    export const portafolioData = {
      header: {
        title: 'Portafolio',
        subTitle:'Todos mis proyectos se encuentran en GiLab',
        icon:gitlab,
        link:'https://gitlab.com/jeyban37'
      },}


      export const footerData = {
        header: {
          title: 'Product',
          subTitle:'Hecho por Esteban Getial',
          text:'E Y J',
          final:'©2023',
         
          
        },}

      

     