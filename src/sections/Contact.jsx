import React from 'react'
import { ContactData } from '../data/data.js';
import ItemContact from './../sections/microcomponents/ItemContact.jsx';

const Contact = () => {
  return (
  
   <section id='contact' className='pt-[200px] pb-[150px] font-roboto bg-custom-mobile-five lg: bg-custom-contact'>
   
   <section className='flex justify-center text-white '>
        <aside className=' flex flex-col max-w-[680px] text-center items-center'>
          <h1 className='font-semibold text-5xl'>{ContactData.header.title}</h1>
          <p className='mt-2 text-xs text-[#A4A4A4]'>
          {ContactData.header.subTitle}
          </p>

        
        </aside>
        </section>
        <ul className='flex flex-col justify-center gap-6 px-8 py-4 mt-6 lg:px-14 lg:gap-4 lg:flex-row lg:mt-7 '>
          {ContactData.items.map((item) => (
            <ItemContact data={item} />
          ))}
        </ul>
    </section>
   
)}
export default Contact;