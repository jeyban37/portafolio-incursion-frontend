import React from 'react'
import { HeroData } from '../data/data'
import { LayoutHero,LayoutHeroMo } from '../sections/microcomponents/Layout';
import Temporal from './../assets/images/Temporal.png';


const Hero = () => {
  return (
    <section id='hero' className='pt-[200px] pb-[150px] font-roboto bg-custom-mobile lg: bg-custom-hero '>
     
      <LayoutHero >
      <section id='hero' className=' flex flex-col items-center lg:flex-row lg:justify-around lg:items-end'>
          <aside className=' text-center lg:text-left'>
            <section className='px-4 lg:w-[488px] lg:px-0'>
              <h1 className='font-semibold text-2xl text-white'>
              {HeroData.title}
              </h1>
              <p className='px-10 text-base text-white lg:px-0'>
              {HeroData.subTitle}
              </p>
            </section>
            
          </aside>
Esteban Estuvo Aqui      
        </section>
      </LayoutHero>

    <div className='mt-4'></div>
      <LayoutHeroMo >
        
<div className=' flex flex-col gap-2 '>


  <div className=' flex flex-row gap-2'> 
  
    <div className=' basis-1/2 bg-blue-700  h-40 rounded-sm '>
      <a href={HeroData.linkMe}>
        <div className=' flex justify-center py-11'><img src={HeroData.Image} alt="" className='w-16'/></div></a>
    </div>


   <div className='flex flex-col flex-1 gap-2   '> 

   <a href={HeroData.linkContact}>
         <div className='bg-blue-400 h-20 rounded-sm'><div className=' flex justify-center py-4'><img src={HeroData.imagetwo} alt="" className='w-10'/></div>
    </div> </a>

    <a href={HeroData.linkSkills}>
          <div className='bg-green-600 h-[75px] rounded-sm'>
          <div className=' flex justify-center py-4'><img src={HeroData.lapiz} alt="" className='w-10'/></div>
        </div>
    </a>

      </div>
  </div>
  
  
  <div className=' flex flex-row flex-1 gap-2  '> 

      <div className=' basis-5/12'> 
      
      <a href={HeroData.linkExperience}>
       <div className='bg-yellow-600 h-40 rounded-sm'>
       <div className=' flex justify-center py-11'><img src={HeroData.camio} alt="" className='w-16'/></div>
        </div></a>
        
        </div>

       
      <div className=' flex-1 ' >
      <a href={HeroData.linkBriefcase}>
          <div div className='bg-red-700 h-40 rounded-sm '>
          <div className=' flex justify-center py-11'><img src={HeroData.portfolio} alt="" className='w-16'/></div>
            </div>
          </a>
        </div>
  
  </div>
  
</div>

</LayoutHeroMo>
    </section>
   
  )
}

export default Hero
