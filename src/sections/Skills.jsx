import React from 'react';
import {skillsData} from '../data/data'
import Carrusel from './microcomponents/Carrucel';



const Skills = () => {
  return (
    <section id='skills' className='pt-[200px] pb-[150px] font-roboto bg-custom-mobile-three lg: bg-custom-skills'>
      
      <section className='flex justify-center text-white '>
        <aside className='max-w-[680px] text-center'>
          <h1 className='font-semibold text-5xl'>{skillsData.header.title}</h1>
          <p className='mt-4 text-xs text-[#A4A4A4]'>
          {skillsData.header.subTitle}
          </p>
        </aside>
      </section>
<div className=''> <Carrusel/></div>
     


           

    </section>
  );
};

export default Skills;
