import React from 'react'
import { MeData } from '../data/data'
import { LayoutHero } from '../sections/microcomponents/Layout';


const Me = () => {
  return (
    <section id='me' className='pt-[200px] pb-[150px] font-roboto bg-custom-mobile lg: bg-custom-me'>
       <LayoutHero>
        <section className='flex flex-col items-center lg:flex-row lg:justify-between lg:items-end'>
          <aside className='text-center lg:text-left'>
            <section className='px-4 lg:w-[488px] lg:px-0'>
            <h1 className='font-semibold text-xs text-[#A4A4A4]'>
                {MeData.note}
              </h1>
              <h1 className='font-semibold text-5xl text-white'>
                {MeData.title}
              </h1>
              <p className='px-10 mt-4 text-lg text-white lg:px-0'>
              {MeData.text}
              </p>
            </section>
            <section className='flex flex-col gap-y-4 justify-center mt-12 px-6 sm:px-0 sm:gap-y-0 sm:gap-x-4 sm:flex-row lg:justify-start'>
             
            </section>
          </aside>
          <img
            src={MeData.me.icon}
            alt={MeData.me.title}
            className=' rounded-3xl max-w-[350px]  shadow-2xl  py-4 sm: mt-10  '
          />
        </section>
      </LayoutHero>
    </section>
   
    
  )
}

export default Me
