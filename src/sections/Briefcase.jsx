import React from 'react'
import { portafolioData } from '../data/data'


const Briefcase = () => {
  return (
<section id='briefcase' className='pt-[200px] pb-[150px] font-roboto bg-custom-mobile-six lg: bg-custom-briefcase '>

      <section className='flex justify-center text-white '>
        <aside className=' flex flex-col max-w-[680px] text-center items-center'>
          <h1 className='font-semibold text-5xl'>{portafolioData.header.title}</h1>
          <p className='mt-4 text-xs text-[#A4A4A4]'>
          {portafolioData.header.subTitle}
          </p>


          <a href={portafolioData.header.link}>  <img
            src={portafolioData.header.icon}
            alt={portafolioData.header.title}
            className=' mt-8 w-40 '
          /></a>

        
        </aside>
        
      </section>
    
    
    
    </section>
  )
}

export default Briefcase