import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import ItemsCarrusel from './ItemsCarrusel.jsx';
import { skillsData } from '../../data/data';


const Carrusel = () => {
    const settings = {
      dots: true,
      infinite: true,
      speed: 800,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
  
    return (


      <div  className=" px-20 carousel-container ">
      <Slider {...settings} className='flex flex-row'>
     
          {skillsData.items.map((item) => (
            <ItemsCarrusel data={item} />
          ))}
     
     
       
      </Slider>

      </div>

      
    );
  };
  export default Carrusel;