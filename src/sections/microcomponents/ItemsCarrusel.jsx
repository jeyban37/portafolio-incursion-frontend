import React from 'react';

const ItemsCarrusel = ({ data }) => {


  return (

    
    
    <article className='flex flex-col items-center justify-center'>
              <div className='flex flex-col items-center gap-y-6 w-[300px]'>
                <div className='w-20 h-20 text-white' />
                <img src={data.icon} alt={data.title} className='h-20' />
                <p className='text-center text-white w-56 lg:w-72'>
                    {data.subTitle}
                </p>
              </div>
            </article>

   
  );
};

export default ItemsCarrusel;