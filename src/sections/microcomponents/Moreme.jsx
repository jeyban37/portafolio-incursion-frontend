import React, { useState } from 'react';
import { MeData } from '../../data/data';

const Moreme = () => {
  const [ampliado, setAmpliado] = useState(false);

  const handleAmpliarTexto = () => {
    setAmpliado(!ampliado);
  };

  return (
    <div className="">
      <p className={` text-white text-2xl font-medium px-10  lg:flex justify-center  lg:text-2xl ${ampliado ? 'text-lg' : 'text-sm '}`}>
    {MeData.text}
         
      </p>
      <button
        className=" flex text-[#3563E9] font-normal py-2 px-11 lg:"
        onClick={handleAmpliarTexto}
      >
        <h1 className='mt-1'>{ampliado ? 'Reducir' : 'Leer mas'}</h1>
        <img src={MeData.fl.icon} alt={MeData.fl.title} className='h-6 mt-1' />
      </button>
    </div>
  );
};
export default Moreme;