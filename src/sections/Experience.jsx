import React from 'react'
import { LayoutHero } from '../sections/microcomponents/Layout';
import { ExperienceData } from '../data/data';
import ItemService from '../sections/microcomponents/ItemService';
const Experience = () => {
  return (
 
    <section id='experience' className='pt-[200px] pb-[150px] font-roboto bg-custom-mobile-four lg: bg-custom-experience'>
    
    <LayoutHero>
        <section className='flex flex-col items-center lg:flex-row lg:justify-between lg:items-end'>
          <aside className='text-center lg:text-left  py-8'>
            <section className='px-4 lg:w-[488px] lg:px-0'>
            
              <h1 className='font-semibold text-3xl text-white px-5 lg:px-0'>{ExperienceData.header.title}
              </h1>
              <p className='px-10 text-xs text-[#A4A4A4] lg:px-0'>
                 {ExperienceData.header.subTitle}
              </p>

              <img
            src={ExperienceData.header.icon}
            alt = {ExperienceData.header.title}
            className='rounded-3xl shadow-2xl  sm: mt-10 '
          />
            </section>
            <section className='flex flex-col gap-y-4 justify-center mt-12 px-6 sm:px-0 sm:gap-y-0 sm:gap-x-4 sm:flex-row lg:justify-start'>
             
            </section>
          </aside>

          <section>
        <ul className='flex flex-col gap-3 px-8 lg:px-14 lg:gap-4 '>
          {ExperienceData.items.map((item) => (
            <ItemService data={item} />
          ))}
        </ul>
      </section>
         
        

        </section>
      </LayoutHero>
    
    </section>
   
    
     
   
   
    
  )
}

export default Experience
