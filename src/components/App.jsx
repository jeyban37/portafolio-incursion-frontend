import React from "react"; 
import Header from "../sections/Header";
import Hero from "../sections/Hero";
import Me from "../sections/Me";
import Experience from "../sections/Experience";
import Contact from "../sections/Contact";
import Skills from "../sections/Skills";
import Briefcase from "../sections/Briefcase";
import Footer from "../sections/Footer";



 

function App() {


  return (
  <>
  <body className="bg-portfolio-primary-950 h-full w-full" >
   
  <Header/>
  <Hero/>
  <Me/>
  <Skills/>
  <Briefcase/>
  <Experience/>
  <Contact/>
   <Footer/>
  </body>


  </>
  
     
  );
}

export default App;